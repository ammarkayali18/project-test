<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    // https://g.tenor.com/v1/search?q=energy&key=LIVDSRZULELA&limit=10

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $endpointUrl;
    public function __construct()
    {
        $this->middleware('auth');
        $this->endpointUrl = config('links.gif_api', "https://api.thecatapi.com/v1/images/search");
    }

    /**
     * Show gifs for category all.
     *
     * @return [GIF] results 
     */
    public function index()
    {

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->endpointUrl, [
            'query' => [
                "category_names" => "all",
                "mime_types" => "gif",
                "limit" => 30,

            ],
        ]);

        $results =  json_decode($response->getBody(), true);
        return view('home', compact('results'));
    }

    /**
     * Show gifs by categoryName parameter.
     *
     * @param String categoryName 
     * @return [GIF] data 
     */
    public function search($categoryName)
    {

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->endpointUrl, [
            'query' => [
                "category_names" => $categoryName,
                "mime_types" => "gif",
                "limit" => 30,
            ],
        ]);
        $response = json_decode($response->getBody(), true);
        return  $response;
    }
}
