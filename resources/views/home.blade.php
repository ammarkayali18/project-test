@extends('layouts.app')

@section('content')
<style>
    .wrapper {
        display: grid;
        grid-template-columns: repeat(16, 1fr);
        gap: 10px;
        grid-auto-rows: minmax(100px, auto);
    }

    #gif {
        width: 200px;
        height: 200px;
        margin: 4px
    }

    .one {
        grid-column: 2 / 16;
        grid-row: 1;
        /* border-radius: 10px red; */
    }

    #category_name {
        background-color: #3CBC8D;
        color: white;
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        border-radius: 15px 50px 30px 5px;

    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">



                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success " role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="input-group mb-3">

                        <input type="text" class="form-control" placeholder="Search by category name" name="category_name" id="category_name" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="wrapper">
                        <div id="one" class="one"> @foreach($results as $gif)


                            <img id="gif" src="{{$gif['url']}}" alt="this slowpoke moves" />


                            @endforeach
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#category_name").keyup(function() {
        document.querySelectorAll('#gif').forEach(e => e.remove());
        $.ajax({
            url: "/search-gif/" + $("#category_name").val,
            type: 'GET',
            dataType: 'json',
            success: function(res) {
                for (let i = 0; i < res.length; i++) {

                    divOne = document.getElementById("one");
                    var img = document.createElement("img");
                    img.src = res[i]["url"];
                    img.id = "gif";

                    divOne.appendChild(img);
                }


            }
        });

    });
</script>
@endsection