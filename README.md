# multi-level
 multi level category. 

### Install
*  Make sure to create a new `.env` file, you can copy the content from `.env.example`,then do the database configurations .
*  you can import database from multi_level.sql file, without run php artisan migrate command.
*  Run the following commands:
    ```shell script
    composer install 
    php artisan migrate
    php artisan key:generate
    ```

